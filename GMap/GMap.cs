﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SourceCode.Forms.Controls.Web.SDK;
using SourceCode.Forms.Controls.Web.SDK.Attributes;

[assembly: WebResource("GMap.Script.js", "text/javascript", PerformSubstitution = true)]

namespace GMap
{
	//definition file
	[ControlTypeDefinition("GMap.Definition.xml")]
	[ClientScript("GMap.Script.js")]

	public class MyControl : BaseControl
	{
		public MyControl() : base("div")
        {
		}

		public string ControlName
		{
			get { return this.Attributes["ControlName"]; }
			set { this.Attributes["ControlName"] = value; }
		}
		public string Width
		{
			get { return this.Attributes["width"]; }
			set { Console.WriteLine("CS Width");  this.Attributes["width"] = value; }
		}
		public string Height
		{
			get { return this.Attributes["height"]; }
			set { Console.WriteLine("CS Height");  this.Attributes["height"] = value; }
		}
		public string Longitude
		{
			get { return this.Attributes["Longitude"]; }
			set { this.Attributes["Longitude"] = value.ToString(); }
		}
		public string Latitude
		{
			get { return this.Attributes["Latitude"]; }
			set { this.Attributes["Latitude"] = value.ToString(); }
		}
		public bool IsVisible
		{
			get { return this.GetOption<bool>("isvisible", true); }
			set { this.SetOption<bool>("isvisible", value, true); }
		}
		public bool IsEnabled
		{
			get { return this.GetOption<bool>("isenabled", true); }
			set { this.SetOption<bool>("isenabled", value, true); }
		}

		/*
		protected override void CreateChildControls()
		{
			base.EnsureChildControls();

			switch (base.State)
			{
				case SourceCode.Forms.Controls.Web.Shared.ControlState.Designtime:
					this.ID = Guid.NewGuid().ToString();
					break;
				case SourceCode.Forms.Controls.Web.Shared.ControlState.Preview:
					break;
				case SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime:
					this.Attributes.Add("enabled", this.IsEnabled.ToString());
					this.Attributes.Add("visible", this.IsVisible.ToString());
					this.Attributes.Add("Width", this.Width);
					this.Attributes.Add("Height", this.Height);
					this.Attributes.Add("Latitude", this.Latitude);
					this.Attributes.Add("Longitude", this.Longitude);
					break;
			}

			base.CreateChildControls();
		}
		*/

		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			LiteralControl ctrl = new LiteralControl();

			switch (base.State)
			{
				case SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime:
					Console.WriteLine("render");
					ctrl.Text = GenerateHtml();
					break;

				default:
					ctrl.Text = GenerateHtml();
					break;
			}

			ctrl.RenderControl(writer);
		}

		/*
		//disable cacheing
		protected override void OnInit(EventArgs e) { Page.Response.Cache.SetCacheability(HttpCacheability.NoCache); base.OnInit(e); }

		protected override void CreateChildControls()
		{
			//this.Attributes.CssStyle.Add("width", "100%");
			Panel pnlDevSurface = new Panel();
			pnlDevSurface.BorderStyle = BorderStyle.Solid;
			pnlDevSurface.Height = Unit.Pixel(Convert.ToInt32(this.Height));
			pnlDevSurface.Width = Unit.Pixel(Convert.ToInt32(this.Width));
			pnlDevSurface.Controls.Add(new LiteralControl(GenerateHtml()));
			this.Controls.Add(pnlDevSurface);

			this.Attributes.Add("height", Height);
			this.Attributes.Add("width", Width);

			base.CreateChildControls();
		}
		*/

		/*
		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			writer.Write(string.Format(GenerateHtml()));
		}
		*/

		private string GenerateHtml()
		{
			string LiteralString;
			string MapAddress;
			MapAddress = @"https://www.google.com/maps/embed/v1/place?key=AIzaSyAMDaDSjv_rUpei66cFHiWe4RKFaAvAA8k &q=" + this.Longitude + @"," + this.Latitude;
			LiteralString = @"<iframe width =" + Width + @" height=" + Height + @" frameborder=""0"" style=""border:0"" src=" + '"' + MapAddress + '"' + @" allowfullscreen></iframe> ";
			return LiteralString;
		}
	}
}