﻿(function ($) {
	if (typeof GMap === "undefined" || GMap == null) GMap = {};

	GMap = {
		_getInstance: function (id) {
			var control = jQuery('#' + id);
			if (control.length == 0) {
				throw 'GMap \'' + id + '\' not found';
			} else {
				return control[0];
			}
		},

		execute: function (objInfo) {
			var method = objInfo.methodName;
			if (method) {
				var _map = $("#" + objInfo.CurrentControlID);
				switch (method) {
					case "Refresh":
						console.log("Refresh");
						break;
					case "TestMethod":
						alert("Test Method");
						console.log("Test method");
						break;
				}
			}
		},

		getValue: function (objInfo) {
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			return instance.value;
		},

		setValue: function (objInfo) {
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			instance.value = objInfo.Value;
			//$("#" + objInfo.CurrentControlId).val(objInfo.Value);
			raiseEvent(objInfo.CurrentControlId, 'Control', 'OnChange');
		},

		getProperty: function (objInfo)
		{
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			return instance.attributes[objInfo.property].value;
		},

		setProperty: function (objInfo) {
			switch (objInfo.property.toLowerCase()) {
				case "isvisible":
					GMap.setIsVisible(objInfo);
					break;
				case "isenabled":
					GMap.setIsEnabled(objInfo);
					break;
				case "isreadonly":
					GMap.SetControlIsReadOnly(objInfo);
					break;
				case "width":
					GMap.setWidth(objInfo);
					break;
				case "height":
					GMap.setHeight(objInfo);
					break;
				default:
					var instance = GMap._getInstance(objInfo.CurrentControlId);
					instance.attributes[objInfo.property].value = objInfo.Value;
			}
		},

		setHeight: function (objInfo) {
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			instance.attributes[objInfo.property].value = objInfo.Value;
			console.log(objInfo.property, instance.attributes["height"].value);
		},

		setWidth: function (objInfo) {
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			instance.attributes[objInfo.property].value = objInfo.Value;
			console.log(objInfo.property, instance.attributes["width"].value);
		},

		setIsVisible: function (objInfo) {
			value = (objInfo.Value === true || objInfo.Value == 'true');
			this._isVisible = value;
			var displayValue = (value === false) ? "none" : "block";
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			instance.style.display = displayValue;
		},

		SetControlIsReadOnly: function (objInfo) {
			value = (objInfo.Value === true || objInfo.Value == 'true');
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			instance.attributes["isreadonly"].value = value;
		},

		setIsEnabled: function (objInfo) {
			value = (objInfo.Value === true || objInfo.Value == 'true');
			this._isEnabled = value;
			var instance = GMap._getInstance(objInfo.CurrentControlId);
			instance.readOnly = !value;
		}
	};
})(jQuery);

$(document).ready(function () {
	$(document).delegate('.SFC.GMap-MyControl', 'click.Control', function (e) {
		raiseEvent(this.id, 'Control', 'OnClick');
	});

	$(document).delegate(".SFC.GMap-MyControl", "change.Control", function (e) {
		raiseEvent(this.id, 'Control', 'OnChange');
		console.log("OnChange");
	});
});